<?php

/**
 * @file
 * Definition of views_handler_filter_views_published_or_roles_has_roles.
 */

/**
 * Filter by published status and by role.
 *
 * @ingroup views_filter_handlers
 */
class views_handler_filter_views_published_or_roles_has_roles extends views_handler_filter {

  function can_expose() { return FALSE; }

  function get_value_options() {
    $options = user_roles(TRUE);
    unset($options[DRUPAL_AUTHENTICATED_RID]);

    return $options;
  }

  function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#type' => 'select',
      '#title' => 'Select Role(s)',
      '#options' => $this->get_value_options(),
      '#default_value' => $this->value,
      '#multiple' => TRUE,
    );
  }

  function query() {
    $table = $this->ensure_my_table();
    $table = 'node';
    $roles = array_keys($this->value);
    $this->query->add_where_expression(
      $this->options['group'],
      "$table.status = 1
      OR
      ($table.uid = ***CURRENT_USER*** AND ***CURRENT_USER*** <> 0 AND ***VIEW_OWN_UNPUBLISHED_NODES*** = 1)
      OR
      ***BYPASS_NODE_ACCESS*** = 1
      OR
      ***CURRENT_USER*** IN (SELECT users_roles.uid FROM users_roles WHERE users_roles.rid IN (:roles))", array(':roles' => $roles));
  }
}
